import Asset from '@/services/Asset';
import Base from '@/services/Base';
import DarkMode from '@/services/DarkMode';
import PrivateMode from '@/services/PrivateMode';
import RestApi from '@/services/RestApi';
import {Chart as ChartJs} from 'chart.js';
import {ref} from 'vue';

export interface PortfolioHoldingsChartInterface {
  assetCode: string;
  assetColor: string;
  assetValuePct: number;
}

export default abstract class Chart {
  public static readonly portfolioHoldingsChartData = ref<PortfolioHoldingsChartInterface[]>([]);
  public static readonly portfolioBalanceChartData = ref<any[]>([]);
  public static readonly portfolioAssetBalanceChartData = ref<any[]>([]);
  private static readonly enabledChartIndex = ref<number>(0);
  private static readonly portfolioCanvas = ref<any>();
  private static readonly portfolioItemCanvas = ref<any>();

  public static async enablePortfolioBalanceChart() {
    this.enabledChartIndex.value = 0;
    await this.refresh();
  }

  public static async enablePortfolioHoldingsChart() {
    this.enabledChartIndex.value = 1;
    await this.refresh();
  }

  public static async enablePortfolioItemBalanceChart() {
    this.enabledChartIndex.value = 2;
    await this.refresh();
  }

  public static deletePortfolioBalanceChart() {
    this.portfolioBalanceChartData.value = [];
    this.portfolioHoldingsChartData.value = [];
  }

  public static deletePortfolioItemBalanceChart() {
    this.portfolioAssetBalanceChartData.value = [];
  }

  public static getEnabledChartIndex(): number {
    return this.enabledChartIndex.value;
  }

  public static async refreshAll() {
    this.portfolioBalanceChartData.value = [];
    this.portfolioHoldingsChartData.value = [];
    this.portfolioAssetBalanceChartData.value = [];
    await this.refresh();
  }

  public static async refresh() {
    switch (this.enabledChartIndex.value) {
      case 0:
        return this.renderPortfolioBalanceChart();
      case 1:
        return this.renderPortfolioHoldingsChart();
      case 2:
        return this.renderPortfolioItemBalanceChart();
    }
  }

  private static async renderPortfolioBalanceChart() {
    if (this.portfolioBalanceChartData.value.length === 0) {
      this.portfolioBalanceChartData.value = await RestApi.getPortfolioBalanceChart();
    }

    const darkModeEnabled = DarkMode.isEnabled();
    const darkColor = darkModeEnabled ? '#ffffff' : '#000000';
    const mediumLightColor = darkModeEnabled ? '#404040' : '#bfbfbf';
    const lineColors: string[] = darkModeEnabled ? ['#00ba9e', '#006ecd', '#5cc140'] : ['#00ba9e', '#006ecd', '#5cc140'];

    const portfolioCanvasRef = document.getElementById('portfolio-canvas') as HTMLCanvasElement;
    if (portfolioCanvasRef) {
      const labels: string[] = [];
      const data: number[][] = [[], []];
      if (this.portfolioBalanceChartData.value) {
        for (const item of this.portfolioBalanceChartData.value) {
          if (!PrivateMode.isEnabled()) {
            const d = item['value0'].slice(8, 10);
            const m = item['value0'].slice(5, 7);
            labels.push(parseInt(d) + '. ' + parseInt(m) + '.');
            data[0].push(item['value1']);
            data[1].push(item['value2']);
          }
        }
      }
      const datasets: any = [];
      datasets.push({label: 'Data 1', data: data[0], borderColor: lineColors[0], borderWidth: 2, fill: false, pointRadius: 0});
      datasets.push({label: 'Data 2', data: data[1], borderColor: lineColors[1], borderWidth: 2, fill: false, pointRadius: 0});
      if (this.portfolioCanvas.value) {
        this.portfolioCanvas.value.destroy();
      }
      const options = {
        type: 'line',
        data: {labels: labels, datasets: datasets},
        options: {
          aspectRatio: 1.5,
          legend: {display: false},
          animation: {duration: 0},
          scales: {
            xAxes: [{gridLines: {display: false}, ticks: {fontColor: darkColor}}],
            yAxes: [{
              display: !PrivateMode.isEnabled(),
              gridLines: {display: true, color: mediumLightColor, zeroLineColor: mediumLightColor},
              ticks: {
                fontColor: darkColor,
                callback: (value: any) => {
                  return Base.printNumber(parseFloat(value.toString()));
                },
              },
            }],
          },
          tooltips: {
            enabled: false,
          },
        },
      };
      this.portfolioCanvas.value = new ChartJs(portfolioCanvasRef, options);
    }
  }

  private static async renderPortfolioHoldingsChart() {
    if (this.portfolioHoldingsChartData.value.length === 0) {
      this.portfolioHoldingsChartData.value = await RestApi.getPortfolioHoldingsChart();
    }

    const darkModeEnabled = DarkMode.isEnabled();
    const darkColor = darkModeEnabled ? '#ffffff' : '#000000';
    const lineColors: string[] = [];
    const labels: string[] = [];
    const data: number[] = [];
    for (const item of this.portfolioHoldingsChartData.value) {
      lineColors.push(item.assetColor);
      labels.push(item.assetCode + ': ' + Base.printPct(item.assetValuePct, 2));
      data.push(item.assetValuePct);
    }
    const portfolioCanvasRef = document.getElementById('portfolio-canvas') as HTMLCanvasElement;
    if (portfolioCanvasRef) {
      if (this.portfolioCanvas.value) {
        this.portfolioCanvas.value.destroy();
      }
      this.portfolioCanvas.value = new ChartJs(portfolioCanvasRef, {
        type: 'doughnut',
        data: {
          labels,
          datasets: [{
            data,
            borderWidth: 0,
            borderColor: 'transparent',
            backgroundColor: lineColors,
          }],
        },
        options: {
          aspectRatio: 1.5,
          legend: {
            position: 'bottom',
            fullWidth: true,
            labels: {
              fontColor: darkColor,
              boxWidth: 8,
              padding: 16,
              usePointStyle: true,
            },
            onClick: undefined,
          },
          tooltips: {
            enabled: false,
          },
          animation: {duration: 0},
        },
      });
    }
  }

  private static async renderPortfolioItemBalanceChart() {
    if (this.portfolioAssetBalanceChartData.value.length === 0) {
      const selectedAsset = Asset.getSelected();
      if (selectedAsset) {
        this.portfolioAssetBalanceChartData.value = await RestApi.getPortfolioAssetBalanceChart(selectedAsset.backendId);
      }
    }

    const darkModeEnabled = DarkMode.isEnabled();
    const darkColor = darkModeEnabled ? '#ffffff' : '#000000';
    const mediumLightColor = darkModeEnabled ? '#404040' : '#bfbfbf';
    const lineColors: string[] = darkModeEnabled ? ['#00ba9e', '#006ecd', '#5cc140'] : ['#00ba9e', '#006ecd', '#5cc140'];

    const portfolioItemCanvasRef = document.getElementById('portfolio-item-canvas') as HTMLCanvasElement;
    if (portfolioItemCanvasRef) {
      const labels: string[] = [];
      const data: number[][] = [[], []];
      if (this.portfolioAssetBalanceChartData.value) {
        for (const item of this.portfolioAssetBalanceChartData.value) {
          if (!PrivateMode.isEnabled()) {
            const d = item['value0'].slice(8, 10);
            const m = item['value0'].slice(5, 7);
            labels.push(parseInt(d) + '. ' + parseInt(m) + '.');
            data[0].push(item['value1']);
            data[1].push(item['value2']);
          }
        }
      }
      const datasets: any = [];
      datasets.push({label: 'Data 1', data: data[0], borderColor: lineColors[0], borderWidth: 2, fill: false, pointRadius: 0});
      datasets.push({label: 'Data 2', data: data[1], borderColor: lineColors[1], borderWidth: 2, fill: false, pointRadius: 0});
      datasets.push({label: 'Data 3', data: data[2], borderColor: lineColors[2], borderWidth: 2, fill: false, pointRadius: 0});
      if (this.portfolioItemCanvas.value) {
        this.portfolioItemCanvas.value.destroy();
      }
      const options = {
        type: 'line',
        data: {labels: labels, datasets: datasets},
        options: {
          aspectRatio: 1.5,
          legend: {display: false},
          animation: {duration: 0},
          scales: {
            xAxes: [{gridLines: {display: false}, ticks: {fontColor: darkColor}}],
            yAxes: [{
              display: !PrivateMode.isEnabled(),
              gridLines: {display: true, color: mediumLightColor, zeroLineColor: mediumLightColor},
              ticks: {
                fontColor: darkColor,
                callback: (value: any) => {
                  return Base.printNumber(parseFloat(value.toString()));
                },
              },
            }],
          },
          tooltips: {
            enabled: false,
          },
        },
      };
      this.portfolioItemCanvas.value = new ChartJs(portfolioItemCanvasRef, options);
    }
  }
}
