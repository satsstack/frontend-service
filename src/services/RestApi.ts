import Asset from '@/services/Asset';
import Base from '@/services/Base';
import {PortfolioHoldingsChartInterface} from '@/services/Chart';
import Currency from '@/services/Currency';
import {PortfolioAssetInterface, PortfolioAssetTransactionInterface, PortfolioInterface} from '@/services/Portfolio';
import User, {UserInterface} from '@/services/User';
import '@capacitor-community/http';
import {HttpParams} from '@capacitor-community/http';
import {Plugins} from '@capacitor/core';

const REST_API_BASE_URL_USER = 'https://users.satsstack.app/rest/api/user';
const REST_API_BASE_URL_TRANSACTION = 'https://transactions.satsstack.app/rest/api/transaction';
const REST_API_BASE_URL_ANALYSIS = 'https://analysis.satsstack.app/rest/api/statistics';

const {Http} = Plugins;

export default abstract class RestApi {

  public static async getUser(): Promise<UserInterface> {
    const responseData: UserInterface = {token: '', id: '', email: '', profileImagePath: ''};
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_USER + '/get/token', headers: this.getHeaders()});
      if (response.status === 200 && response.data && response.data.id) {
        //console.log('Received data', response.data);
        responseData.id = response.data.id;
        responseData.email = response.data.email;
        responseData.profileImagePath = response.data.userPic;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPortfolioAssets(): Promise<PortfolioAssetInterface[]> {
    const responseData: PortfolioAssetInterface[] = [];
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/view/asset/' + User.get().id, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        for (const item of response.data) {
          const assetCode = item['asset']['shortCut'].toUpperCase();
          if (assetCode) {
            const assetKey = Asset.getIndexByCode(assetCode);
            if (assetKey > -1) {
              responseData.push({
                code: assetCode,
                name: Asset.getName(assetKey),
                logo: Asset.getLogoPathByKey(assetKey),
                price: item['price'],
                holdings: item['holdingQuantity'],
                balance: item['holdingValue'],
                profit: item['profitLossValue'],
                profitPct: item['profitLossPercentage'] / 100,
              });
            }
          }
        }
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPortfolioAsset(assetId: number) {
    const responseData: PortfolioAssetTransactionInterface[] = [];
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_TRANSACTION + '/find/asset/' + User.get().id + '/' + assetId, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        for (const item of response.data) {
          const assetCode = item['asset']['shortCut'].toUpperCase();
          if (assetCode) {
            const assetKey = Asset.getIndexByCode(assetCode);
            if (assetKey > -1) {
              responseData.push({
                id: item['id'],
                type: this.getTransactionType(item['transactionType']),
                datetime: Base.getDatetime(item['date']),
                price: item['pricePerAsset'],
                amount: item['quantity'],
                amountCur: item['quantity'] * item['pricePerAsset'],
                fee: item['fee'] / item['pricePerAsset'],
                feeCur: item['fee'],
                note: item['note'],
              });
            }
          }
        }
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async addTransaction(assetIndex: number, type: number, datetime: string, amount: number, price: number, fee: number, note: string) {
    const selectedAsset = Asset.get(assetIndex);
    const requestData = {
      transactionType: this.getBackendTransactionType(type),
      date: datetime,
      quantity: amount,
      pricePerAsset: price,
      fee: fee,
      note: note,
      asset: {
        id: selectedAsset.backendId,
        name: selectedAsset.name,
        shortCut: selectedAsset.code,
      },
      userId: User.get().id,
    };
    //console.log('Request data', requestData);
    const responseData: PortfolioInterface = {};
    try {
      const response = await Http.request({method: 'POST', url: REST_API_BASE_URL_TRANSACTION + '/save', headers: this.getPostHeaders(), data: requestData});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async editTransaction(id: number, assetIndex: number, type: number, datetime: string, amount: number, price: number, fee: number, note: string) {
    const selectedAsset = Asset.get(assetIndex);
    const requestData = {
      id: id,
      transactionType: this.getBackendTransactionType(type),
      date: datetime,
      quantity: amount,
      pricePerAsset: price,
      fee: fee,
      note: note,
      asset: {
        id: selectedAsset.backendId,
        name: selectedAsset.name,
        shortCut: selectedAsset.code,
      },
      userId: User.get().id,
    };
    //console.log('Request data', requestData);
    const responseData: PortfolioInterface = {};
    try {
      const response = await Http.request({method: 'PUT', url: REST_API_BASE_URL_TRANSACTION + '/update', headers: this.getPostHeaders(), data: requestData});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async deleteTransaction(id: number): Promise<boolean> {
    let responseData = false;
    try {
      const response = await Http.request({method: 'DELETE', url: REST_API_BASE_URL_TRANSACTION + '/delete/' + id, headers: this.getHeaders()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        responseData = true;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async deleteAsset(id: number): Promise<boolean> {
    let responseData = false;
    try {
      const response = await Http.request({method: 'DELETE', url: REST_API_BASE_URL_TRANSACTION + '/delete/' + User.get().id + '/' + id, headers: this.getHeaders()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        responseData = true;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPortfolio(): Promise<{ totalBuy: number; totalSell: number }> {
    const responseData = {totalBuy: 0, totalSell: 0};
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/invested/' + User.get().id, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        responseData.totalBuy = response.data;
      }
      const response2 = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/sell/' + User.get().id, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response2.status === 200 && response2.data) {
        //console.log('Received data', response2.data);
        responseData.totalSell = response2.data;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPortfolioItem(assetBackendId: number): Promise<{ totalBuy: number; totalSell: number }> {
    const responseData = {totalBuy: 0, totalSell: 0};
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/invested/asset/' + User.get().id + '/' + assetBackendId, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        responseData.totalBuy = response.data;
      }
      const response2 = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/sell/' + User.get().id + '/' + assetBackendId, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response2.status === 200 && response2.data) {
        //console.log('Received data', response2.data);
        responseData.totalSell = response2.data;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPrice(selectedAssetIndex: number): Promise<number> {
    const selectedAssetName = Asset.getName(selectedAssetIndex);
    let responseData = 0;
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/price/' + selectedAssetName, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        responseData = response.data;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPortfolioBalanceChart(): Promise<any[]> {
    let responseData: any[] = [];
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/graph/line/' + User.get().id, headers: this.getHeaders(), params: this.getCurrencyParameters()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        responseData = response.data;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPortfolioAssetBalanceChart(assetBackendId: number): Promise<any[]> {
    let responseData: any[] = [];
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/graph/line/' + User.get().id, headers: this.getHeaders(), params: {...this.getCurrencyParameters(), ...{assetId: assetBackendId.toString()}}});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        responseData = response.data;
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  public static async getPortfolioHoldingsChart(): Promise<PortfolioHoldingsChartInterface[]> {
    const responseData: PortfolioHoldingsChartInterface[] = [];
    try {
      const response = await Http.request({method: 'GET', url: REST_API_BASE_URL_ANALYSIS + '/graph/holding/' + User.get().id, headers: this.getHeaders()});
      if (response.status === 200 && response.data) {
        //console.log('Received data', response.data);
        for (const assetCode in response.data) {
          if (Object.prototype.hasOwnProperty.call(response.data, assetCode)) {
            const assetKey = Asset.getIndexByCode(assetCode);
            if (assetKey > -1) {
              const assetColor = Asset.getColor(assetKey);
              const assetValuePct = response.data[assetCode] / 100;
              responseData.push({assetCode, assetColor, assetValuePct});
            }
          }
        }
      }
    } catch (e) {
      console.error(e);
    }
    //console.log('Response data', responseData);
    return responseData;
  }

  private static getHeaders() {
    return {
      'Authorization': User.get().token,
    };
  }

  private static getPostHeaders() {
    return {
      'Authorization': User.get().token,
      'Content-Type': 'application/json',
    };
  }

  private static getBackendTransactionType(value: number): string {
    switch (value) {
      case 0:
        return 'BUY';
      case 1:
        return 'SELL';
    }
    return '';
  }

  private static getTransactionType(value: string): number {
    switch (value) {
      case 'BUY':
        return 0;
      case 'SELL':
        return 1;
    }
    return -1;
  }

  private static getCurrencyParameters(): HttpParams {
    const currencyCode = Currency.getSelected().code;
    if (currencyCode === 'USD') {
      return {};
    } else {
      return {currency: Currency.getSelected().code};
    }
  }
}
