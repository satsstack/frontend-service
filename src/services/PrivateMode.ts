import Chart from '@/services/Chart';
import {Plugins} from '@capacitor/core';
import {ref} from 'vue';

const {Storage} = Plugins;
const STORAGE_VARIABLE = 'privateModeEnabled';

export default abstract class PrivateMode {
  private static readonly enabled = ref<boolean>(false);

  public static async initialize() {
    await this.load();
  }

  public static isEnabled(): boolean {
    return this.enabled.value;
  }

  public static async toggle() {
    this.enabled.value = !this.enabled.value;
    await Chart.refresh();
    await this.save();
  }

  private static async load() {
    const ret = await Storage.get({key: STORAGE_VARIABLE});
    if (ret.value) {
      this.enabled.value = JSON.parse(ret.value);
    }
  }

  private static async save() {
    await Storage.set({key: STORAGE_VARIABLE, value: JSON.stringify(this.enabled.value)});
  }
}
