import Chart from '@/services/Chart';
import {Plugins} from '@capacitor/core';
import {ref} from 'vue';

const {Storage} = Plugins;
const STORAGE_VARIABLE = 'darkModeEnabled';
const DARK_MODE_BODY_CLASS = 'dark';

export default abstract class DarkMode {
  private static readonly enabled = ref<boolean>(false);

  public static async initialize() {
    await this.load();
    await this.apply();
  }

  public static isEnabled(): boolean {
    return this.enabled.value;
  }

  public static async toggle() {
    this.enabled.value = !this.enabled.value;
    await this.apply();
    await Chart.refresh();
    await this.save();
  }

  private static async apply() {
    document.body.classList.toggle(DARK_MODE_BODY_CLASS, this.enabled.value);
    const metaThemeColor = document.querySelector('meta[name=theme-color]');
    if (metaThemeColor) {
      metaThemeColor.setAttribute('content', this.enabled.value ? '#1e1e1e' : '#ffffff');
    }
  }

  private static async load() {
    const ret = await Storage.get({key: STORAGE_VARIABLE});
    if (ret.value) {
      this.enabled.value = JSON.parse(ret.value);
    }
  }

  private static async save() {
    await Storage.set({key: STORAGE_VARIABLE, value: JSON.stringify(this.enabled.value)});
  }
}
