import Asset from '@/services/Asset';
import RestApi from '@/services/RestApi';
import {ref} from 'vue';

export interface PortfolioInterface {
  totalBalance?: number;
  totalInvested?: number;
  totalProfit?: number;
  totalProfitPct?: number;
}

export interface PortfolioAssetInterface {
  code?: string;
  name?: string;
  logo?: string;
  price?: number;
  holdings?: number;
  balance?: number;
  profit?: number;
  profitPct?: number | null;
}

export interface PortfolioAssetTransactionInterface {
  id?: number;
  type?: number;
  datetime?: string;
  price?: number;
  amount?: number;
  amountCur?: number;
  fee?: number;
  feeCur?: number;
  note?: string;
}

export default abstract class Portfolio {
  public static totalBalance = ref<number>();
  public static totalInvested = ref<number>();
  public static totalProfit = ref<number>();
  public static totalProfitPct = ref<number | null>();
  public static itemTotalBalance = ref<number>();
  public static itemTotalInvested = ref<number>();
  public static itemTotalProfit = ref<number>();
  public static itemTotalProfitPct = ref<number | null>();
  public static assets = ref<PortfolioAssetInterface[]>([{}, {}, {}, {}]);
  public static transactions = ref<PortfolioAssetTransactionInterface[]>([{}, {}, {}, {}, {}, {}]);
  private static selectedTransaction = ref<PortfolioAssetTransactionInterface>();

  public static async refreshAssets() {
    this.assets.value = await RestApi.getPortfolioAssets();
    const portfolio = await RestApi.getPortfolio();
    this.totalInvested.value = portfolio.totalBuy;
    let totalBalance = 0;
    for (const item of this.assets.value) {
      totalBalance += item.balance ? item.balance : 0;
    }
    if (portfolio.totalBuy) {
      const totalProfit: number = totalBalance - (portfolio.totalBuy - portfolio.totalSell);
      const totalProfitPct: number | null = portfolio.totalBuy > 0 ? totalProfit / portfolio.totalBuy : null;
      this.totalBalance.value = totalBalance;
      this.totalProfit.value = totalProfit;
      this.totalProfitPct.value = totalProfitPct;
    }
  }

  public static async refreshTransactions() {
    const selectedAsset = Asset.getSelected();
    const selectedAssetCode = selectedAsset ? selectedAsset.code : undefined;
    if (selectedAsset && selectedAssetCode) {
      this.transactions.value = await RestApi.getPortfolioAsset(selectedAsset.backendId);
      const portfolio = await RestApi.getPortfolioItem(selectedAsset.backendId);
      this.itemTotalInvested.value = portfolio.totalBuy;
      let totalBalance = 0;
      for (const item of this.transactions.value) {
        if (item.type === 0) {
          totalBalance += item.amount ? item.amount : 0;
        } else if (item.type === 1) {
          totalBalance -= item.amount ? item.amount : 0;
        }
      }
      totalBalance = totalBalance * await RestApi.getPrice(Asset.getIndexByCode(selectedAssetCode));
      if (portfolio.totalBuy) {
        const totalProfit: number = totalBalance - (portfolio.totalBuy - portfolio.totalSell);
        const totalProfitPct: number | null = portfolio.totalBuy > 0 ? totalProfit / portfolio.totalBuy : null;
        this.itemTotalBalance.value = totalBalance;
        this.itemTotalProfit.value = totalProfit;
        this.itemTotalProfitPct.value = totalProfitPct;
      }
    }
  }

  public static deleteAssets() {
    this.totalBalance.value = undefined;
    this.totalInvested.value = undefined;
    this.totalProfit.value = undefined;
    this.totalProfitPct.value = undefined;
    this.assets.value = [{}, {}, {}, {}];
  }

  public static deleteTransactions() {
    this.itemTotalBalance.value = undefined;
    this.itemTotalInvested.value = undefined;
    this.itemTotalProfit.value = undefined;
    this.itemTotalProfitPct.value = undefined;
    this.transactions.value = [{}, {}, {}, {}, {}, {}];
  }

  public static deleteAll() {
    this.deleteAssets();
    this.deleteTransactions();
  }

  public static getTransactionById(id: number): PortfolioAssetTransactionInterface | undefined {
    return this.transactions.value.find(x => x.id === id);
  }

  public static selectTransaction(id: number) {
    this.selectedTransaction.value = this.getTransactionById(id);
  }

  public static getSelectedTransaction(): PortfolioAssetTransactionInterface | undefined {
    return this.selectedTransaction.value;
  }
}
