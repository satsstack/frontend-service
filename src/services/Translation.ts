import Language from '@/services/Language';
import Czech from '@/translations/cs.json';
import Deutsch from '@/translations/de.json';
import English from '@/translations/en.json';
import Russian from '@/translations/ru.json';

const LANGUAGES: { [index: string]: any } = {
  'cs': Czech,
  'en': English,
  'de': Deutsch,
  'ru': Russian,
};

function translate(value: string): string {
  const languageId = Language.getSelected().id;
  return LANGUAGES[languageId][value];
}

export {translate as t};
