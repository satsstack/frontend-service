import Chart from '@/services/Chart';
import Portfolio from '@/services/Portfolio';
import {Plugins} from '@capacitor/core';
import {ref} from 'vue';

const {Storage} = Plugins;
const STORAGE_VARIABLE = 'currencySelectedId';

const CURRENCIES = [
  {
    id: 'usd',
    code: 'USD',
    label: 'CURRENCY_USD_LABEL',
    sign: '$',
  },
  {
    id: 'eur',
    code: 'EUR',
    label: 'CURRENCY_EUR_LABEL',
    sign: '€',
  },
  {
    id: 'czk',
    code: 'CZK',
    label: 'CURRENCY_CZK_LABEL',
    sign: 'Kč',
  },
  {
    id: 'btc',
    code: 'BTC',
    label: 'CURRENCY_BTC_LABEL',
    sign: 'BTC',
  },
];

export interface CurrencyItem {
  id: string;
  code: string;
  label: string;
  sign: string;
}

export default abstract class Currency {
  private static readonly selected = ref<CurrencyItem>(CURRENCIES[0]);

  public static async initialize() {
    await this.load();
  }

  public static getAll(): CurrencyItem[] {
    return CURRENCIES;
  }

  public static getSelected(): CurrencyItem {
    return this.selected.value;
  }

  public static async setByKey(key: number) {
    this.selected.value = CURRENCIES[key];
    await Portfolio.deleteAll();
    await Portfolio.refreshAssets();
    await Portfolio.refreshTransactions();
    await Chart.refresh();
    await this.save();
  }

  private static async load() {
    const ret = await Storage.get({key: STORAGE_VARIABLE});
    if (ret.value) {
      const selectedId = JSON.parse(ret.value);
      const selected: CurrencyItem | undefined = CURRENCIES.find(item => item.id == selectedId);
      if (selected) {
        this.selected.value = selected;
      }
    }
  }

  private static async save() {
    await Storage.set({key: STORAGE_VARIABLE, value: JSON.stringify(this.selected.value.id)});
  }
}
