import Currency, {CurrencyItem} from '@/services/Currency';
import Language, {LanguageItem} from '@/services/Language';

export default abstract class Base {
  public static printNumber(value?: any, fixed?: number, forceSign?: boolean, currencySign?: string): string {
    if (value === null) {
      return '';
    }
    const languageItem: LanguageItem = Language.getSelected();
    const currencyItem: CurrencyItem = Currency.getSelected();
    const currAppend = languageItem.currAppend || currencyItem.code === currencyItem.sign || currencySign;
    value = value ? parseFloat(value) : 0;
    fixed = fixed && fixed > 0 ? fixed : 0;
    forceSign = value && !!forceSign;
    currencySign = currencySign ? currencySign : currencyItem.sign;
    let result = '';
    const sign: boolean = Math.sign(value) === -1;
    result += sign ? '- ' : forceSign ? '+ ' : '';
    result += !currAppend ? currencySign : '';
    result += Math.abs(value).toLocaleString(languageItem.locale, {minimumFractionDigits: fixed, maximumFractionDigits: fixed});
    result += currAppend ? ' ' + currencySign : '';
    return result;
  }

  public static printPct(value?: any, fixed?: number, forceSign?: boolean) {
    if (value === null) {
      return '';
    }
    value = value ? parseFloat(value) * 100 : 0;
    const languageItem: LanguageItem = Language.getSelected();
    let result = '';
    const sign: boolean = Math.sign(value) === -1;
    result += sign ? '- ' : forceSign ? '+ ' : '';
    result += Math.abs(value).toLocaleString(languageItem.locale, {minimumFractionDigits: fixed, maximumFractionDigits: fixed});
    result += (languageItem.pctSpace ? ' ' : '') + '%';
    return result;
  }

  public static printSignColor(value?: any): string {
    if (value === null) {
      return '';
    }
    value = value ? parseFloat(value) : 0;
    return value ? (Math.sign(value) === -1 ? 'color-danger' : 'color-success') : '';
  }

  public static printDate(datetime: string) {
    const date = new Date(datetime.slice(0, 19));
    return date.getDate() + '. ' + (date.getMonth() + 1) + '. ' + date.getFullYear();
  }

  public static printTime(datetime: string) {
    const date = new Date(datetime.slice(0, 19));
    return this.appendZero(date.getHours()) + ':' + this.appendZero(date.getMinutes());
  }

  public static printDatetime(datetime: any) {
    return this.printDate(datetime) + ' ' + this.printTime(datetime);
  }

  public static getDatetime(datetime: string) {
    const date = new Date(datetime);
    return date.getFullYear() + '-' + this.appendZero(date.getMonth() + 1) + '-' + this.appendZero(date.getDate()) + 'T' + this.appendZero(date.getHours()) + ':' + this.appendZero(date.getMinutes()) + ':00';
  }

  public static getNumber(value: string): number {
    return parseFloat(value.toString().replace(',', '.'));
  }

  private static appendZero(value: number) {
    return value < 10 ? '0' + value : value;
  }
}
