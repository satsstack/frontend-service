import {ref} from 'vue';

const ASSETS = [
  {
    id: 'bnb',
    code: 'BNB',
    name: 'Binance Coin',
    color: '#f3ba2f',
    backendId: 2,
  },
  {
    id: 'btc',
    code: 'BTC',
    name: 'Bitcoin',
    color: '#f7931a',
    backendId: 1,
  },
  {
    id: 'bch',
    code: 'BCH',
    name: 'Bitcoin Cash',
    color: '#8dc351',
    backendId: 3,
  },
  {
    id: 'ada',
    code: 'ADA',
    name: 'Cardano',
    color: '#0033ad',
    backendId: 4,
  },
  {
    id: 'link',
    code: 'LINK',
    name: 'Chainlink',
    color: '#375bd2',
    backendId: 5,
  },
  {
    id: 'doge',
    code: 'DOGE',
    name: 'Dogecoin',
    color: '#f9c325',
    backendId: 6,
  },
  {
    id: 'eth',
    code: 'ETH',
    name: 'Ethereum',
    color: '#3c3c3d',
    backendId: 7,
  },
  {
    id: 'ltc',
    code: 'LTC',
    name: 'Litecoin',
    color: '#345d9d',
    backendId: 8,
  },
  {
    id: 'dot',
    code: 'DOT',
    name: 'Polkadot',
    color: '#e6007a',
    backendId: 9,
  },
  {
    id: 'xlm',
    code: 'XLM',
    name: 'Stellar',
    color: '#000000',
    backendId: 10,
  },
  {
    id: 'usdt',
    code: 'USDT',
    name: 'Tether',
    color: '#50af95',
    backendId: 11,
  },
  {
    id: 'uni',
    code: 'UNI',
    name: 'Uniswap',
    color: '#ff007a',
    backendId: 12,
  },
  {
    id: 'usdc',
    code: 'USDC',
    name: 'USD Coin',
    color: '#0075cf',
    backendId: 13,
  },
  {
    id: 'vet',
    code: 'VET',
    name: 'VeChain',
    color: '#00bfff',
    backendId: 14,
  },
  {
    id: 'xrp',
    code: 'XRP',
    name: 'XRP',
    color: '#23292f',
    backendId: 15,
  },
];

export interface AssetItem {
  id: string;
  code: string;
  name: string;
  color: string;
  backendId: number;
}

export default abstract class Asset {
  private static readonly selected = ref<AssetItem>();

  public static getAll(): AssetItem[] {
    return ASSETS;
  }

  public static getSelected(): AssetItem | undefined {
    return this.selected.value;
  }

  public static setByIndex(index: number) {
    this.selected.value = ASSETS[index];
  }

  public static get(index: number) {
    return ASSETS[index];
  }

  public static getIndexByCode(code: string): number {
    return ASSETS.findIndex(x => x.code === code);
  }

  public static getCode(index: number): string {
    return ASSETS[index].code;
  }

  public static getName(index: number): string {
    return ASSETS[index].name;
  }

  public static getColor(index: number): string {
    return ASSETS[index].color;
  }

  public static getLogoPathByKey(key: number): string {
    return '/assets/crypto/' + ASSETS[key].id + '.png';
  }
}
