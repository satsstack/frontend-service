import {createRouter, createWebHistory} from '@ionic/vue-router';
import {RouteRecordRaw} from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import ('@/pages/Home.vue'),
  },
  {
    path: '/portfolio',
    component: () => import ('@/pages/Portfolio.vue'),
  },
  {
    path: '/portfolio/:code',
    component: () => import ('@/pages/PortfolioItem.vue'),
  },
  {
    path: '/login',
    component: () => import ('@/pages/Login.vue'),
  },
  {
    path: '/login/callback',
    component: () => import ('@/pages/LoginCallback.vue'),
  },
  {
    path: '/logout',
    component: () => import ('@/pages/Logout.vue'),
  },
  {
    path: '/:x',
    component: () => import ('@/pages/Home.vue'),
  },
];

const Router = createRouter({history: createWebHistory(process.env.BASE_URL), routes});
export default Router;
