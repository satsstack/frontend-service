import RestApi from '@/services/RestApi';
import {Plugins} from '@capacitor/core';
import CryptoES from 'crypto-es';
import {ref} from 'vue';

const {Storage} = Plugins;
const STORAGE_VARIABLE = 'user';

export interface UserInterface {
  token: string;
  id: string;
  email: string;
  profileImagePath: string;
}

export default abstract class User {
  private static readonly user = ref<UserInterface>({token: '', id: '', email: '', profileImagePath: ''});

  public static async initialize() {
    await this.load();
  }

  public static get(): UserInterface {
    return this.user.value;
  }

  public static async logIn(token: string): Promise<boolean> {
    if (token) {
      this.user.value.token = token;
      if (await this.loadData()) {
        await this.save();
        return true;
      }
    }
    return false;
  }

  public static async logOut() {
    this.user.value = {token: '', id: '', email: '', profileImagePath: ''};
    await this.save();
  }

  public static isLoggedIn(): boolean {
    return !!this.user.value.token;
  }

  private static async load() {
    const ret = await Storage.get({key: STORAGE_VARIABLE});
    if (ret.value) {
      this.user.value = JSON.parse(ret.value);
      await this.loadData();
    }
  }

  private static async loadData(): Promise<boolean> {
    if (this.user.value.token) {
      const user = await RestApi.getUser();
      if (user && user.id) {
        this.user.value.id = user.id;
        if (user.email) {
          this.user.value.email = user.email;
          if (user.profileImagePath) {
            this.user.value.profileImagePath = user.profileImagePath;
          } else {
            this.user.value.profileImagePath = 'https://www.gravatar.com/avatar/' + CryptoES.MD5(user.email) + '?d=identicon&s=64';
          }
        }
        return true;
      }
    }
    return false;
  }

  private static async save() {
    await Storage.set({key: STORAGE_VARIABLE, value: JSON.stringify(this.user.value)});
  }
}
