import Chart from '@/services/Chart';
import {Plugins} from '@capacitor/core';
import {ref} from 'vue';

const {Storage} = Plugins;
const STORAGE_VARIABLE = 'languageSelectedId';

const LANGUAGES = [
  {
    id: 'en',
    code: 'EN',
    label: 'English',
    locale: 'en-US',
    currAppend: false,
    pctSpace: false,
  },
  {
    id: 'cs',
    code: 'CS',
    label: 'Čeština',
    locale: 'cs-CZ',
    currAppend: true,
    pctSpace: true,
  },
  {
    id: 'de',
    code: 'DE',
    label: 'Deutsche',
    locale: 'de-DE',
    currAppend: true,
    pctSpace: true,
  },
  {
    id: 'ru',
    code: 'RU',
    label: 'русский',
    locale: 'ru-RU',
    currAppend: true,
    pctSpace: true,
  },
];

export interface LanguageItem {
  id: string;
  code: string;
  label: string;
  locale: string;
  currAppend: boolean;
  pctSpace: boolean;
}

export default abstract class Language {
  private static readonly selected = ref<LanguageItem>(LANGUAGES[0]);

  public static async initialize() {
    await this.load();
  }

  public static getAll(): LanguageItem[] {
    return LANGUAGES;
  }

  public static getSelected(): LanguageItem {
    return this.selected.value;
  }

  public static async setByKey(key: number) {
    this.selected.value = LANGUAGES[key];
    await Chart.refresh();
    await this.save();
  }

  private static async load() {
    const ret = await Storage.get({key: STORAGE_VARIABLE});
    if (ret.value) {
      const selectedId = JSON.parse(ret.value);
      const selected: LanguageItem | undefined = LANGUAGES.find(item => item.id == selectedId);
      if (selected) {
        this.selected.value = selected;
      }
    }
  }

  private static async save() {
    await Storage.set({key: STORAGE_VARIABLE, value: JSON.stringify(this.selected.value.id)});
  }
}
