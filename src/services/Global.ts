import Currency from '@/services/Currency';
import DarkMode from '@/services/DarkMode';
import Language from '@/services/Language';
import PrivateMode from '@/services/PrivateMode';
import User from '@/services/User';

export default abstract class Global {
  public static async initializeAll() {
    await DarkMode.initialize();
    await PrivateMode.initialize();
    await Language.initialize();
    await Currency.initialize();
    await User.initialize();
  }
}
