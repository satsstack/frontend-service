// https://docs.cypress.io/api/introduction/api.html

describe('Login page check', () => {
  it('Visits the app root url', () => {
    cy.visit('http://localhost:8080/login')
  })

  it('Check dark mode', () => {
    cy.get('ion-content.md').should('have.css', 'background', 'rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box')
    cy.get('.ion-float-end > :nth-child(2)').click()
    cy.get('ion-content.md').should('have.css', 'background', 'rgb(18, 18, 18) none repeat scroll 0% 0% / auto padding-box border-box')
  })

  it('Check translate', () => {
    cy.get('ion-card-header.ion-inherit-color > .ion-inherit-color').contains('Log in')
    cy.get('.ion-float-end > :nth-child(1)').click()
    cy.get(':nth-child(2) > .sc-ion-label-md-h').click()
    cy.get('ion-card-header.ion-inherit-color > .ion-inherit-color').contains('Přihlásit se')
  })

  it('Are login buttons visible?', () => {
    cy.get('.facebook-button')
    cy.get('.google-button')
  })

  it('Video end wait', () => {
    cy.wait(20000)
  })
})
