# SatsStack

This is a cross-platform web application built on [Ionic Framework 5](https://ionicframework.com/) and [Capacitor 2](https://capacitorjs.com/).

## Production

The application is currently running at [https://satsstack.app/](https://satsstack.app/).

## Installation

1. Prepare [Node.js](https://nodejs.org/en/download/), [npm](https://www.npmjs.com/get-npm) and [Ionic CLI](https://ionicframework.com/docs/cli).
1. Run `npm install` (installs all dependencies)
1. Run `ionic serve` (serves application for development)

## Docker

*Replace `IMAGE_NAME` and `PORT` in next commands.*

1. Run `docker build -t IMAGE_NAME .` (builds application image)
1. Run `docker run -p PORT:3333 IMAGE_NAME` (serves application on selected port)

## License

[The Unlicense](LICENSE.md)
